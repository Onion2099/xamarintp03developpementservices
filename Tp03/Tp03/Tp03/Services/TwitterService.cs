﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tp03.Domains.Models.Services;
using Tp03.Domains.Models.DbModels;

namespace Tp03.Services
{
    public class TwitterService : ITwitterService
    {
        public List<Tweet> Tweets
        {
            get { User user = new User() { Login = "userLog", Password = "userMdp" };
                
                return new List<Tweet>()
                {
                    new Tweet(){User = user, Data ="Message1", CreatedAt = DateTime.Now},
                    new Tweet(){User = user, Data ="Message2", CreatedAt = DateTime.Now},
                    new Tweet(){User = user, Data ="Message3", CreatedAt = DateTime.Now},
                    new Tweet(){User = user, Data ="Message4", CreatedAt = DateTime.Now}
                };
            }
        }

        public Boolean Authenticate(User user)
        {
            return Tweets.Select(x => x.User).Any(x => x.Login == user.Login && x.Password == user.Password);
        }
    }
}