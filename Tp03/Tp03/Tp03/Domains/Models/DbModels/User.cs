﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tp03.Domains.Models.DbModels
{
	public class User
	{
		private String login;
		private String password;
		private String email;

		public String Login
		{
			get { return login; }
			set { login = value; }
		}

		public String Password
		{
			get { return password; }
			set { password = value; }
		}

		public String Email
		{
			get { return email; }
			set { email = value; }
		}
	}
}